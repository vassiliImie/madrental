package com.formation.madrental.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.formation.madrental.R
import com.formation.madrental.activity.DetailActivity
import com.formation.madrental.activity.MainActivity
import com.formation.madrental.classe.Vehicule
import com.formation.madrental.fragment.DetailFragment
import com.squareup.picasso.Picasso

class VehiculeAdapter (private var listeVehicule: MutableList<Vehicule>,private val mainActivity: MainActivity) : RecyclerView.Adapter<VehiculeAdapter.VehiculeViewHolder>() {

    companion object{
        const val libelleCategorie : String = "Categorie Co2 : "
        const val libellePrixJournalier : String = "€ / jour"
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VehiculeAdapter.VehiculeViewHolder {
        val viewCourse = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_vehicule, parent, false)
        return VehiculeViewHolder(viewCourse)
    }

    override fun getItemCount(): Int = listeVehicule.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: VehiculeAdapter.VehiculeViewHolder, position: Int) {
        holder.textViewTitre.text = listeVehicule[position].nom
        holder.textViewPrix.text = listeVehicule[position].prixjournalierbase.toString() + libellePrixJournalier
        holder.textViewCategory.text = libelleCategorie+listeVehicule[position].categorieco2

        Picasso.get()
            .load("http://s519716619.onlinehome.fr/exchange/madrental/images/"+listeVehicule[position].image)
            .into(holder.imageViewVehicule)
    }

    // ViewHolder :
    inner class VehiculeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        val textViewTitre: TextView = itemView.findViewById(R.id.titre_vehicule)
        val textViewPrix: TextView = itemView.findViewById(R.id.prix)
        val textViewCategory : TextView = itemView.findViewById(R.id.categorie)
        val imageViewVehicule : ImageView= itemView.findViewById(R.id.image_vehicule)


        init {

            itemView.setOnClickListener {

                if (mainActivity.findViewById<FrameLayout>(R.id.conteneur_detail) != null)
                {
                    // mode tablette :
                    val fragment = DetailFragment()
                    val bundle = Bundle()
                    bundle.putSerializable("vehicule",listeVehicule[adapterPosition])
                    fragment.arguments = bundle

                    val transaction: FragmentTransaction = mainActivity.supportFragmentManager.beginTransaction()
                    transaction.replace(R.id.conteneur_detail, fragment)
                    transaction.commit()

                }else{
                    val intent = Intent(itemView.context, DetailActivity::class.java)
                    intent.putExtra("vehicule",listeVehicule[adapterPosition])
                    itemView.context.startActivity(intent)
                }
            }
        }
    }

}