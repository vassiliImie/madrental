package com.formation.madrental.database

import android.content.Context
import androidx.room.Room

class AppDatabaseHelper(context: Context) {

    companion object{

        private lateinit var databaseHelper: AppDatabaseHelper

        fun getDatabase(context: Context) : AppDatabase
        {
            if(!Companion::databaseHelper.isInitialized){
                databaseHelper = AppDatabaseHelper(context)
            }
            return databaseHelper.database
        }
    }

    // Base de données :
    val database: AppDatabase = Room
        .databaseBuilder(context.applicationContext, AppDatabase::class.java, "vehicules.db")
        .allowMainThreadQueries()
        .build()

}