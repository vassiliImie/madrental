package com.formation.madrental.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.formation.madrental.classe.Vehicule
import com.formation.madrental.dao.VehiculeDao

@Database(entities = [Vehicule::class], version = 1)
abstract class AppDatabase : RoomDatabase(){
    abstract fun vehiculeDao() : VehiculeDao
}