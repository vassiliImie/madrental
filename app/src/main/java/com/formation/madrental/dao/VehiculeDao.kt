package com.formation.madrental.dao

import androidx.room.*
import com.formation.madrental.classe.Vehicule

/**
 * Classe dao de requetage pour le vehicule
 */
@Dao
abstract class VehiculeDao {

    @Query("SELECT * FROM vehicule")
    abstract fun getAllvehicule() : List<Vehicule>

    @Query("SELECT COUNT(*) FROM vehicule WHERE nom = :nom")
    abstract fun countByNom(nom : String) : Long

    @Query("SELECT * FROM vehicule WHERE id = :idApi")
    abstract fun getVehiculeByIdApi(idApi : Double) : Vehicule

    @Transaction
    @Insert
    abstract fun insert(vararg Vehicule: Vehicule)

    @Transaction
    @Update
    abstract fun update(vararg Vehicule: Vehicule)

    @Transaction
    @Delete
    abstract fun delete(vararg Vehicule: Vehicule)
}