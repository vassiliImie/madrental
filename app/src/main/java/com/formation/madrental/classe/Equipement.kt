package com.formation.madrental.classe

import java.io.Serializable


/**
 * Modele Equipement reflet du retour Web Service
 */
data class Equipement (
    val id : Double,
    val nom :String
) : Serializable
