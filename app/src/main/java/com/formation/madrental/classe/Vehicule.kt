package com.formation.madrental.classe

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.io.Serializable

/**
 * Entité vehicule
 */
@Entity(tableName = "vehicule")
data class Vehicule (
    @PrimaryKey(autoGenerate = true)
    val idVehivule :Int,
    val id : Double,
    val nom : String,
    val image : String,
    val disponible : Double,
    val prixjournalierbase : Double,
    val promotion : Double,
    val agemin : Double,
    val categorieco2 : String

):Serializable {
    @Ignore
    val equipements : List<Equipement> = ArrayList<Equipement>()

    @Ignore
    val options : List<OptionsVehicule> = ArrayList<OptionsVehicule>()
}