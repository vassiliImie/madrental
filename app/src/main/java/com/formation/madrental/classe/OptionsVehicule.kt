package com.formation.madrental.classe

import java.io.Serializable


/**
 * Modele OptionsVehicule reflet du retour Web Service
 */
data class OptionsVehicule (
    val id : Double,
    val nom : String,
    val prix : Double
): Serializable
