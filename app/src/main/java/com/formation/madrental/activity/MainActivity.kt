package com.formation.madrental.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Switch
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.formation.madrental.R
import com.formation.madrental.adapter.VehiculeAdapter
import com.formation.madrental.classe.Vehicule
import com.formation.madrental.service.VehiculeService
import com.formation.madrental.ws.WsVehicule
import com.formation.madrental.ws.retrofit.RetrofitSingleton
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private var listeVehicule: MutableList<Vehicule> = ArrayList()
    private val vehiculeAdapter = VehiculeAdapter(listeVehicule,this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // -- INITIALISATION --
        liste_vehicule.setHasFixedSize(true)
        initLayoutManager()

        // Appel ws
        onRechercherVehicules()
    }

    /**
     * Appel le web service pour aller chercher les vehicules
     */
    fun onRechercherVehicules() {
        listeVehicule.clear()

        val service = RetrofitSingleton.retrofit.create(WsVehicule::class.java)
        val call = service.wsGet()
        call.enqueue(object : Callback<List<Vehicule>>
        {
            override fun onResponse(call: Call<List<Vehicule>>, response: Response<List<Vehicule>>) {

                if (response.isSuccessful)
                {
                    val results = response.body()

                    if (results != null) {
                        listeVehicule.addAll(results)
                        vehiculeAdapter.notifyDataSetChanged()
                    } else {
                        Log.e("Requête Web service",getString(R.string.error_no_vehicle_returned))
                    }
                }
            }

            override fun onFailure(call: Call<List<Vehicule>>, t: Throwable) {
                Log.e("Requête Web service","${t.message}")
            }
        })
    }

    /**
     * Initialisation du layout manager pour le RecyclerView
     */
    private fun initLayoutManager() {
        val layoutManager = LinearLayoutManager(this)
        liste_vehicule.layoutManager = layoutManager
        liste_vehicule.adapter = vehiculeAdapter
    }

    /**
     * Comportemant du switch
     *
     * Activé -> affichage favoris
     * Désactivé -> Api
     */
    fun onAfficherCacherFavoris(view: View) {
        val vService = VehiculeService(view.context)
        listeVehicule.clear()

        //Activer
        if(switch_mad_cars.isChecked){
            listeVehicule.addAll(vService.getAll())
            vehiculeAdapter.notifyDataSetChanged()
        }else{
            //Desactiver
            onRechercherVehicules()
        }
    }

}