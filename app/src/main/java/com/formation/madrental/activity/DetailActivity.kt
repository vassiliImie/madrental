package com.formation.madrental.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.formation.madrental.R
import com.formation.madrental.classe.Vehicule
import com.formation.madrental.fragment.DetailFragment
import com.formation.madrental.service.VehiculeService

class DetailActivity : AppCompatActivity() {

    var vehicule : Vehicule? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        // récupération de l'argument
        vehicule = intent.getSerializableExtra("vehicule") as Vehicule

        // Initalisation
        initFragment(vehicule!!)
    }

    private fun initFragment(vehicule : Vehicule) {
        val fragment = DetailFragment()
        val bundle = Bundle()
        bundle.putSerializable("vehicule", vehicule)
        fragment.arguments = bundle

        // transaction
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.conteneur_detail, fragment)
        transaction.commit()
    }
}