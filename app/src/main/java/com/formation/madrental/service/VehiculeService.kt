package com.formation.madrental.service

import android.content.Context
import com.formation.madrental.adapter.VehiculeAdapter
import com.formation.madrental.classe.Vehicule
import com.formation.madrental.dao.VehiculeDao
import com.formation.madrental.database.AppDatabaseHelper
import java.lang.StringBuilder

/**
 * Classe de service pour les vehicules
 */
class VehiculeService(private val context: Context) {

    /**
     * Ajoute un vehicule en base de données
     */
    fun ajouterVehicule( vehicule : Vehicule){
        getVehiculeDao().insert(vehicule)
    }

    /**
     * Retourne tous les vehicules en base de données
     */
    fun getAll(): List<Vehicule> {
        return getVehiculeDao().getAllvehicule()
    }

    /**
     * Recherche dans la base un vehicule avec l'id récupéré dans l'API
     */
    fun getVehiculeByIdApi(idApi : Double) : Vehicule{
        return getVehiculeDao().getVehiculeByIdApi(idApi)
    }

    fun getDetail(vehicule: Vehicule): String {
        // Création du detail
        val detailVehicule =  StringBuilder()
            .append("Nom : ").append(vehicule.nom).append("\n\r")
            .append("Disponible : ").append(vehicule.disponible).append("\n\r")
            .append("Prix journalier : ").append(vehicule.prixjournalierbase).append("\n\r")
            .append("Promotion: ").append(vehicule.promotion).append("\n\r")
            .append("Age min : ").append(vehicule.agemin).append(VehiculeAdapter.libellePrixJournalier).append("\n\r")
            .append("Libelle categorie : ").append(VehiculeAdapter.libelleCategorie).append(vehicule.categorieco2).append("\n\r")
            .append("Equipement").append("\n\r")

        for (equipement in vehicule.equipements) {
            detailVehicule.append("Nom de l'équipement : ").append(equipement.nom).append("\n\r")
        }

        detailVehicule.append("Options").append("\n\r")

        for (option in vehicule.options) {
            detailVehicule.append("Nom de l'option : ").append(option.nom).append("\n\r")
            detailVehicule.append("Prix de l'option : ").append(option.prix).append("\n\r")
        }
        return detailVehicule.toString()
    }

    private fun getVehiculeDao(): VehiculeDao {
        return AppDatabaseHelper.getDatabase(context).vehiculeDao()
    }
}