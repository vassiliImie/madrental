package com.formation.madrental.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.formation.madrental.R
import com.formation.madrental.classe.Vehicule
import com.formation.madrental.service.VehiculeService
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_detail.*


class DetailFragment: Fragment(), View.OnClickListener {

    var vehicule : Vehicule? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val v = inflater.inflate(R.layout.fragment_detail, container, false)
        val b: Button = v.findViewById(R.id.bouton_ajout_favoris) as Button
        b.setOnClickListener(this)

        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val vService : VehiculeService? = this.context?.let { VehiculeService(it) }

        val arguments = requireArguments()
        vehicule = arguments.getSerializable("vehicule") as Vehicule

        vehicule.let {
            if (vService != null) {
                detail_vehicule.text = vService.getDetail(vehicule!!)
            }

            Picasso.get()
                .load("http://s519716619.onlinehome.fr/exchange/madrental/images/${vehicule!!.image}")
                .into(image_vehicule_detail)

        }
    }

    /**
     * Ajoute au favoris en base de données
     */
    fun onAjouterFavoris(view: View) {
        val vService = VehiculeService(view.context)

        if(vehicule != null){
            val vehiculeExistant = vService.getVehiculeByIdApi(vehicule!!.id)
            if(vehiculeExistant == null){
                Toast.makeText(view.context,getString(R.string.vehicle_added_to_database), Toast.LENGTH_LONG).show()
                vService.ajouterVehicule(vehicule!!)
            }else{
                Toast.makeText(view.context,getString(R.string.vehicle_already_in_favorites), Toast.LENGTH_LONG).show()
            }
        } else {
            Log.e("Error", getString(R.string.error_adding_vehicle))
        }
    }

    override fun onClick(view : View?) {
        if (view != null) {
            when (view.id) {
                R.id.bouton_ajout_favoris -> onAjouterFavoris(view)
            }
        }
    }
}