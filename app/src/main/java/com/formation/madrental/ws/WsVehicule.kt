package com.formation.madrental.ws

import com.formation.madrental.classe.Vehicule
import retrofit2.Call
import retrofit2.http.GET

/**
 * Web service Vehicule
 *
 * GET : "get-vehicules" -> Permet de récupérer une liste de véhicules
 *
 */
interface WsVehicule {

    @GET("get-vehicules.php")
    fun wsGet() : Call<List<Vehicule>>
}